﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;//для буфера считывания/записи
using System.Threading;//для ManualResetEvent

namespace BuferizingStreams
{
    public partial class BuferizingStreams : Form
    {
        struct Situation
        {
            public FileStream filestream;
            public byte[] bText;
            public long bufSize;
            public ManualResetEvent manualresetevent;
        }


        public BuferizingStreams()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Нажата кнопка "открыть"
        /// </summary>
        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();//диалоговое окно открытия файла
            string newFileAddress = openFileDialog.FileName;//получение выбранного имени файла

            lbFileName.Text = newFileAddress;
        }


        /// <summary>
        /// Изменился адрес файла
        /// </summary>
        private void lbFileName_TextChanged(object sender, EventArgs e)
        {
            if (lbFileName.Text != "---")
            {
                btRead.Visible = true;
                btWrite.Visible = true;
                btStop.Visible = true;
                richTextBox.Visible = true;
            }
            else
            {
                btRead.Visible = false;
                btWrite.Visible = false;
                btStop.Visible = false;
                richTextBox.Text = "";
                richTextBox.Visible = false;
            }
        }


        private void btRead_Click(object sender, EventArgs e)
        {
            FileStream fstr = new FileStream(lbFileName.Text, FileMode.Open, FileAccess.Read);

            StreamReader streamRd = new StreamReader(fstr);

            //byte[] receivedData = new byte[dataArraySize];

            richTextBox.Text= streamRd.ReadToEnd();

            fstr.Close();
        }


        private void btWrite_Click(object sender, EventArgs e)
        {
            FileStream fstr = new FileStream(lbFileName.Text, FileMode.Create, FileAccess.Write);
            BufferedStream buffStream = new BufferedStream(fstr);
            StreamWriter streamWr = new StreamWriter(buffStream);

            streamWr.WriteLine(richTextBox.Text);

            fstr.Close();
        }
    }
}
